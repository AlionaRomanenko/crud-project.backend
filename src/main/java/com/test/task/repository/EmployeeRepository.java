package com.test.task.repository;

import com.test.task.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    Optional<Employee> findByPhoneNumber(String phoneNumber);
    @Query("SELECT empl FROM Employee empl WHERE empl.id <> :id AND empl.phoneNumber = :phoneNumber ")
    List<Employee> uniquePhoneNumberExceptOwner(Long id, String phoneNumber);
}
