package com.test.task.repository;

import com.test.task.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project,Long> {
        Optional<Project> findByName(String name);
        @Query("SELECT proj FROM Project proj WHERE proj.id <> :id AND proj.name = :name ")
        List<Project> uniqueNameExceptOwner(Long id, String name);
        }
