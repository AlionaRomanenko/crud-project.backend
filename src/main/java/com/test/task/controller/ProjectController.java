package com.test.task.controller;

import com.test.task.dto.EmployeeDTO;
import com.test.task.dto.ProjectDTO;
import com.test.task.service.interfaces.IEmployeeService;
import com.test.task.service.interfaces.IProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectController {

    private final IProjectService projectService;
    private final IEmployeeService employeeService;

    @Autowired
    public ProjectController(IProjectService projectService, IEmployeeService employeeService) {
        this.projectService = projectService;
        this.employeeService = employeeService;
    }


    @PostMapping("/api/project")
    public ResponseEntity addProject(@Valid @RequestBody ProjectDTO projectDTO, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
        }
        projectService.addProject(projectDTO);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/home")
    public ResponseEntity<List<ProjectDTO>> getAllProjects() {

        return ResponseEntity.ok(projectService.getAllProjects());
    }

    @PutMapping(value = "/api/project/{id}")
    public ResponseEntity changeProject(@Valid @RequestBody ProjectDTO projectDTO, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
        }

        projectService.updateProject(projectDTO);

        return ResponseEntity.ok().build();
    }
    @GetMapping(value = "/api/project/{id}/employees")
    public ResponseEntity <List<EmployeeDTO>> getEmployeeByProject(@PathVariable("id") Long id){
        return ResponseEntity.ok(employeeService.getEmployeeByProject(id));
    }
    @DeleteMapping("/api/project/{id}/employee/{id}")
    public ResponseEntity deleteEmployeeFromProject(@Valid @RequestBody EmployeeDTO employeeDTO, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
        }

        employeeService.deleteEmployeeFromProject(employeeDTO);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/project/{id}",method=RequestMethod.DELETE)
    public ResponseEntity deleteProject(@PathVariable("id") Long id) {
        projectService.deleteProject(id);

        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/project/{id}")
    public ResponseEntity<ProjectDTO> getProjectById(@PathVariable("id") Long id){
        return ResponseEntity.ok(projectService.getProjectById(id));
    }


}
