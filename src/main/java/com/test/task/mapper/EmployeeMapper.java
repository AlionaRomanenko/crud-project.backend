package com.test.task.mapper;

import com.test.task.dto.EmployeeDTO;
import com.test.task.entity.Employee;
import org.mapstruct.Mapper;

@Mapper
public interface EmployeeMapper {
    Employee employeeDTOToEmployee(EmployeeDTO employeeDTO);
    EmployeeDTO employeeToEmployeeDTO(Employee employee);
}
