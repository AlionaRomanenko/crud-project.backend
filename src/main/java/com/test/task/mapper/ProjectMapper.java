package com.test.task.mapper;

import com.test.task.dto.ProjectDTO;
import com.test.task.entity.Project;
import org.mapstruct.Mapper;


@Mapper
public interface ProjectMapper {
    Project projectDTOToProject(ProjectDTO projectDTO);
    ProjectDTO projectToProjectDTO(Project project);
}

