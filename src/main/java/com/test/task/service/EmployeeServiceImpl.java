package com.test.task.service;

import com.test.task.dto.EmployeeDTO;
import com.test.task.entity.Employee;
import com.test.task.entity.Project;
import com.test.task.exception.DataBaseException;
import com.test.task.exception.WrongDateException;
import com.test.task.mapper.EmployeeMapper;
import com.test.task.repository.EmployeeRepository;
import com.test.task.repository.ProjectRepository;
import com.test.task.service.interfaces.IEmployeeService;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl  implements IEmployeeService {

    private static final Logger LOGGER= LoggerFactory.getLogger(EmployeeServiceImpl.class);
    private final EmployeeRepository employeeRepository;
    private final ProjectRepository projectRepository;
    private final EmployeeMapper employeeMapper;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, ProjectRepository projectRepository) {
        this.employeeRepository = employeeRepository;
        this.projectRepository = projectRepository;
        this.employeeMapper = Mappers.getMapper(EmployeeMapper.class );
    }

    @Override
    public void addEmployeeToProject(EmployeeDTO employeeDTO){
        LOGGER.info("Check if employee with phone number {} for project with id {} can be created",employeeDTO.getPhoneNumber(),employeeDTO.getId());
        Project project = projectRepository.findById(employeeDTO.getId()).orElseThrow(() ->
                new DataBaseException("Project not found"));
        Employee employee =employeeMapper.employeeDTOToEmployee(employeeDTO);
        project.getEmployees().add(employee);
        employee.getProject().add(project);
        LOGGER.info("Creating employee with phone number {}",employeeDTO.getPhoneNumber());
        employeeRepository.save(employee);
    }

    @Override
    public void addEmployee(EmployeeDTO employeeDTO){
        LOGGER.info("Check if employee with phone number {} can be created",employeeDTO.getPhoneNumber());
        Period diff = Period.between(employeeDTO.getBirthDate(), LocalDate.now());
        if (diff.getYears() < 18){
            throw new WrongDateException("Employee should not be under 18");
        }
        if(employeeRepository.findByPhoneNumber(employeeDTO.getPhoneNumber()).isPresent()){
            throw new DataBaseException("Employee with that phone number already exists");
        }

        LOGGER.info("Creating employee with phone number {}",employeeDTO.getPhoneNumber());
        employeeRepository.save((employeeMapper.employeeDTOToEmployee(employeeDTO)));
    }

    @Override
    public void updateEmployee(EmployeeDTO employeeDTO){
        LOGGER.info("Check if employee with id {} can be updated",employeeDTO.getId());
        Employee employee = employeeRepository.findById(employeeDTO.getId()).orElseThrow(() ->
                new DataBaseException("Employee not found"));
        Period diff = Period.between(employeeDTO.getBirthDate(), LocalDate.now());
        if (diff.getYears() < 18){
            throw new WrongDateException("Employee should not be under 18");
        }
        if(!employeeRepository.uniquePhoneNumberExceptOwner(employeeDTO.getId(), employeeDTO.getPhoneNumber()).isEmpty()){
            throw new DataBaseException("Employee with that phone number already exists");
        }
        LOGGER.info("Updating  employee with id {}",employeeDTO.getId());
        employeeRepository.save(employeeMapper.employeeDTOToEmployee(employeeDTO));

    }

    @Override
    public void deleteEmployee(Long id){
        LOGGER.info("Check if employee with id {} can be deleted",id);
        Employee employee = employeeRepository.findById(id).orElseThrow(() ->
                new DataBaseException("Employee not found"));
        LOGGER.info("Deleting  employee with id {} ",id);
        employeeRepository.delete(employee);
    }

    @Override
    public void deleteEmployeeFromProject(EmployeeDTO employeeDTO){
        LOGGER.info("Check if employee with id {} can be deleted from project",employeeDTO.getId());
        Project project = projectRepository.findByName(employeeDTO.getProjectName()).orElseThrow(() ->
                new DataBaseException("Project not found"));
        Employee employee = employeeRepository.findById(employeeDTO.getId()).orElseThrow(() ->
                new DataBaseException("Project not found"));
        List<Employee> em = project.getEmployees();
        em.remove(employee);
        List<Project> projects = employee.getProject();
        projects.remove(project);
        LOGGER.info("Deleting if employee with id {} can be deleted from project",employeeDTO.getId());
        employeeRepository.save(employee);
    }

    @Override
    public List<EmployeeDTO> getAllEmployees(){
        LOGGER.info("Getting all existing employees");
        return employeeRepository.findAll().stream().map(employeeMapper::employeeToEmployeeDTO).collect(Collectors.toList());
    }

    @Override
    public EmployeeDTO getEmployeeById(Long id){
        LOGGER.info("Getting employee with id {} " ,id);
        Employee employee = employeeRepository.findById(id).orElseThrow(() ->
                new DataBaseException("Employee not found"));
        return employeeMapper.employeeToEmployeeDTO(employee);
    }

    @Override
    public List<EmployeeDTO> getEmployeeByProject(Long id){
        LOGGER.info("Getting all employees from project with id {}" ,id);
        Project project = projectRepository.findById(id).orElseThrow(() ->
                new DataBaseException("Project not found"));

        return project.getEmployees().stream().map(employeeMapper::employeeToEmployeeDTO).collect(Collectors.toList());
    }
}
