package com.test.task.service.interfaces;

import com.test.task.dto.EmployeeDTO;

import java.util.List;

public interface IEmployeeService {
    void addEmployeeToProject(EmployeeDTO employeeDTO);
    void addEmployee(EmployeeDTO employeeDTO);
    void updateEmployee(EmployeeDTO employeeDTO);
    void deleteEmployee(Long id);
    void deleteEmployeeFromProject(EmployeeDTO employeeDTO);
    List<EmployeeDTO> getAllEmployees();
    EmployeeDTO getEmployeeById(Long id);
    List<EmployeeDTO> getEmployeeByProject(Long id);
}
