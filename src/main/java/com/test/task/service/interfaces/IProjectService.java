package com.test.task.service.interfaces;

import com.test.task.dto.ProjectDTO;

import java.util.List;

public interface IProjectService {
    void addProject(ProjectDTO projectDTO);
    List<ProjectDTO> getAllProjects();
    void updateProject(ProjectDTO projectDTO);
    void deleteProject(Long id);
    ProjectDTO getProjectById(Long id);
}
