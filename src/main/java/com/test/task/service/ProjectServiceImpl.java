package com.test.task.service;

import com.test.task.dto.ProjectDTO;
import com.test.task.entity.Project;
import com.test.task.exception.DataBaseException;
import com.test.task.exception.ImpossibleDeleteException;
import com.test.task.exception.WrongDateException;
import com.test.task.mapper.ProjectMapper;
import com.test.task.repository.ProjectRepository;
import com.test.task.service.interfaces.IProjectService;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements IProjectService {

    private static final Logger LOGGER= LoggerFactory.getLogger(ProjectServiceImpl.class);
    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper ;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
        this.projectMapper = Mappers.getMapper(ProjectMapper.class);
    }

    @Override
    public void addProject(ProjectDTO projectDTO){
        LOGGER.info("Check if project with name {} can be created",projectDTO.getName());
        if(projectDTO.getStartDate().isAfter(projectDTO.getEndDate()) ){
            throw new WrongDateException("Start date should be before end date");
        }

        if(projectRepository.findByName(projectDTO.getName()).isPresent()){
            throw new DataBaseException("Project with that name already exists");
        }

        LOGGER.info("Creating project with name {} ",projectDTO.getName());
        projectRepository.save(projectMapper.projectDTOToProject(projectDTO));
    }

    @Override
    public List<ProjectDTO> getAllProjects(){
        LOGGER.info("Getting all existing projects");
       return projectRepository.findAll().stream().map(projectMapper::projectToProjectDTO).collect(Collectors.toList());
    }

    @Override
    public void updateProject(ProjectDTO projectDTO){
        LOGGER.info("Check if project with id {} can be updated",projectDTO.getId());
        Project project1 = projectRepository.findById(projectDTO.getId()).orElseThrow(() ->
                new DataBaseException("Project not found"));
        if(projectDTO.getStartDate().isAfter(projectDTO.getEndDate()) ){
            throw new WrongDateException("Start date should be before end date");
        }

        if(!projectRepository.uniqueNameExceptOwner(projectDTO.getId(),projectDTO.getName()).isEmpty()){
            throw new DataBaseException("Project with that name already exists");
        }

        Project project = projectMapper.projectDTOToProject(projectDTO);
        project.setEmployees(project1.getEmployees());
        LOGGER.info("Updating project with id {}",projectDTO.getId());
        projectRepository.save(project);

    }

    @Override
    public void deleteProject(Long id){
        LOGGER.info("Check if project with id {} can be deleted",id);
        Project project = projectRepository.findById(id).orElseThrow(() ->
                new DataBaseException("Project not found"));
        if(!project.getEmployees().isEmpty()){
            throw new ImpossibleDeleteException("There are employees in project.");
        }
        else{
        LOGGER.info("Deleting employee with id {} can be deleted",id);
        projectRepository.delete(project);
        }
    }

    @Override
    public ProjectDTO getProjectById(Long id){
        LOGGER.info("Getting project with id {} " ,id);
        Project project = projectRepository.findById(id).orElseThrow(() ->
                new DataBaseException("Project not found"));
        return projectMapper.projectToProjectDTO(project);
    }

}
