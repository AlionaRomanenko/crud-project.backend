package com.test.task.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER= LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({DataBaseException.class,ImpossibleDeleteException.class,WrongDateException.class})
    public ResponseEntity handleExceptions(RuntimeException e) {
        LOGGER.warn(e.getMessage());
        return ResponseEntity.status(500).body(e.getMessage());
    }
}
