package com.test.task.exception;

public class ImpossibleDeleteException extends RuntimeException {
    public ImpossibleDeleteException(String message) {
        super(message);
    }
}
