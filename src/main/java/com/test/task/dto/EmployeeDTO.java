package com.test.task.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

public class EmployeeDTO {

    private Long id;

    @NotNull(message = "First name must not be null")
    @Pattern(regexp="^[A-Z][a-zA-Z-]*$", message="Invalid first name")
    private String firstName;

    @NotNull(message = "Last name must not be null")
    @Pattern(regexp="^[A-Z][a-zA-Z-]*$", message="Invalid last name")
    private String lastName;

    @NotNull(message = "Salary must not be null")
    private Double salary;

    @NotNull(message = "Phone number must not be null")
    @Pattern(regexp="^380\\d{9}$", message="Invalid phone number")
    private String phoneNumber;

    @NotNull(message = "BirthDate must not be null")
    private LocalDate birthDate;

    private String projectName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

//    public boolean isActive() {
//        return isActive;
//    }
//
//    public void setActive(boolean active) {
//        isActive = active;
//    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
