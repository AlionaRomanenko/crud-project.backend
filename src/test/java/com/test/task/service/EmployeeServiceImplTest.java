package com.test.task.service;

import com.test.task.dto.EmployeeDTO;
import com.test.task.dto.ProjectDTO;
import com.test.task.entity.Employee;
import com.test.task.entity.Project;
import com.test.task.exception.WrongDateException;
import com.test.task.repository.EmployeeRepository;
import com.test.task.repository.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class EmployeeServiceImplTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private Employee employee;

    @Mock
    private Project project;

    @Spy
    EmployeeDTO employeeDTO = new EmployeeDTO();

    @Spy
    ProjectDTO projectDTO = new ProjectDTO();


    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Test
    void testAddEmployee() {
        employeeDTO.setBirthDate(LocalDate.now().minusYears(18));
        employeeService.addEmployee(employeeDTO);
        verify(employeeRepository, times(1)).save(any(Employee.class));
    }

    @Test
    void testEmployeeWrongDateException() {
        employeeDTO.setBirthDate(LocalDate.now().minusYears(17));
        assertThrows(WrongDateException.class, () -> { employeeService.addEmployee(employeeDTO);
        });
    }

    @Test
    void addEmployeeToProject() {
        ArgumentCaptor<Employee> captor = ArgumentCaptor.forClass(Employee.class);
        String projectName = "First Project";

        EmployeeDTO emp = new EmployeeDTO();
        emp.setId(1L);
        emp.setFirstName("Test");
        emp.setProjectName(projectName);

        Project project = new Project();
        project.setName(projectName);
        project.setId(1L);

        when(projectRepository.findById(1L)).thenReturn(Optional.of(project));
        employeeService.addEmployeeToProject(emp);

        Mockito.verify(employeeRepository).save(captor.capture());
        Employee actual = captor.getValue();
        assertEquals(emp.getFirstName(), actual.getFirstName());
        assertEquals(emp.getId(), actual.getId());
    }


    @Test
    void testUpdateEmployee() {
        employeeDTO.setId(1L);
        projectDTO.setId(1L);
        when(employeeRepository.
                findById(employeeDTO.getId())).thenReturn(Optional.of(employee));
        employeeDTO.setBirthDate(LocalDate.now().minusYears(18));
        employeeService.updateEmployee(employeeDTO);
        verify(employeeRepository, times(1)).save(any(Employee.class));
    }

    @Test
    void testDeleteEmployee() {
        employeeDTO.setId(1L);
        projectDTO.setId(1L);
        when(employeeRepository.
                findById(employeeDTO.getId())).thenReturn(Optional.of(employee));
        employeeService.deleteEmployee(employeeDTO.getId());
        verify(employeeRepository, times(1)).delete(any(Employee.class));
    }

    @Test
    void testGetAllEmployees() {
        employeeService.getAllEmployees();
        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void testGetEmployeeById() {
        employeeDTO.setId(1L);
        when(employeeRepository.
                findById(employeeDTO.getId())).thenReturn(Optional.of(employee));
        employeeService.getEmployeeById(employeeDTO.getId());
        verify(employeeRepository, times(1)).findById(employeeDTO.getId());
    }
}
