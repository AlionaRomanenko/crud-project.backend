package com.test.task.service;

import com.test.task.dto.EmployeeDTO;
import com.test.task.dto.ProjectDTO;
import com.test.task.entity.Employee;
import com.test.task.entity.Project;
import com.test.task.exception.WrongDateException;
import com.test.task.repository.EmployeeRepository;
import com.test.task.repository.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProjectServiceImplTest {
    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private Employee employee;

    @Mock
    private Project project;

    @Spy
    EmployeeDTO employeeDTO = new EmployeeDTO();

    @Spy
    ProjectDTO projectDTO = new ProjectDTO();


    @InjectMocks
    private EmployeeServiceImpl employeeService;


    @Mock
    private List<ProjectDTO> projectDTOList;

    @Mock
    private List<Project> projectList;

    @InjectMocks
    private ProjectServiceImpl projectService;


    @Test
    void testAddProject() {
        projectDTO.setStartDate(LocalDate.now().minusYears(1));
        projectDTO.setEndDate(LocalDate.now());
        projectService.addProject(projectDTO);
        verify(projectRepository, times(1)).save(any(Project.class));
    }

    @Test
    void testProjectWrongDateException() {
        projectDTO.setStartDate(LocalDate.now());
        projectDTO.setEndDate(LocalDate.now().minusYears(1));
        assertThrows(WrongDateException.class, () -> { projectService.addProject(projectDTO);
        });
    }

    @Test
    void testUpdateProject() {
        projectDTO.setId(1L);
        projectDTO.setName("Name");
        projectDTO.setStartDate(LocalDate.now().minusYears(1));
        projectDTO.setEndDate(LocalDate.now());
        when(projectRepository.
                findById(projectDTO.getId())).thenReturn(Optional.of(project));
        employeeDTO.setBirthDate(LocalDate.now().minusYears(18));
        projectService.updateProject(projectDTO);
        verify(projectRepository, times(1)).save(any(Project.class));
    }


    @Test
    void testDeleteProject() {
        projectDTO.setId(1L);
        when(projectRepository.
                findById(projectDTO.getId())).thenReturn(Optional.of(project));
        projectService.deleteProject(projectDTO.getId());
        verify(projectRepository, times(1)).delete(any(Project.class));
    }

    @Test
    void testGetProjectById() {
        projectDTO.setId(1L);
        when(projectRepository.
                findById(projectDTO.getId())).thenReturn(Optional.of(project));
        projectService.getProjectById(projectDTO.getId());
        verify(projectRepository, times(1)).findById(projectDTO.getId());
    }
}
