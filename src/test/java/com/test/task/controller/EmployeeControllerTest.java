package com.test.task.controller;

import com.test.task.dto.EmployeeDTO;
import com.test.task.service.EmployeeServiceImpl;
import com.test.task.service.ProjectServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeServiceImpl employeeService;

    @MockBean
    private ProjectServiceImpl projectService;

    @Test
    public void findAll() throws Exception {
        EmployeeDTO employee = new EmployeeDTO();
        employee.setFirstName("Aliona");
        employee.setId(1L);
        employee.setPhoneNumber("+3804587896");
        List<EmployeeDTO> employees = Arrays.asList(employee);
        given(employeeService.getAllEmployees()).willReturn(employees);
        this.mockMvc.perform(get("/api/employees"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{'id': 1, 'firstName': 'Aliona', 'phoneNumber': '+3804587896'}]"));
    }

    @Test
    public void findById() throws Exception {
        EmployeeDTO employee = new EmployeeDTO();
        employee.setFirstName("Aliona");
        employee.setId(1L);
        employee.setPhoneNumber("+3804587896");
        given(employeeService.getEmployeeById(1L)).willReturn(employee);
        this.mockMvc.perform(get("/api/employee/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{'id': 1, 'firstName': 'Aliona', 'phoneNumber': '+3804587896'}"));
    }



}
