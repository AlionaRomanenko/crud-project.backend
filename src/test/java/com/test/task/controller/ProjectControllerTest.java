package com.test.task.controller;

import com.test.task.dto.ProjectDTO;
import com.test.task.service.EmployeeServiceImpl;
import com.test.task.service.ProjectServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@WebMvcTest
class ProjectControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeServiceImpl employeeService;

    @MockBean
    private ProjectServiceImpl projectService;

    @Test
    public void findAllProjects() throws Exception {
        ProjectDTO project = new ProjectDTO();
        project.setId(1L);
        project.setName("Name");
        List<ProjectDTO> projects = Arrays.asList(project);
        given(projectService.getAllProjects()).willReturn(projects);
        this.mockMvc.perform(get("/api/home"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{'id': 1, 'name': 'Name'}]"));
    }

    @Test
    public void findProjectById() throws Exception {
        ProjectDTO project = new ProjectDTO();
        project.setId(1L);
        project.setName("Name");
        given(projectService.getProjectById(1L)).willReturn(project);
        this.mockMvc.perform(get("/api/project/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{'id': 1, 'name': 'Name'}"));
    }
}
